import time
import itertools
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt
from scipy.spatial.distance import cdist


plt.rcParams['figure.figsize'] = [4, 4]
X = np.array([[0.005*(i//200), 0.005*(i%200)] for i in range(40000)])
Y = np.random.RandomState(42).random_sample((20, 2))


def plot(Xv, Yv, cdist_argmin, metric):
    """Calls `cdist_argmin` and make a Voronoi plot over the mesh."""
    indices = cdist_argmin(Xv,Yv, metric=metric)
    plt.scatter(Xv[:,0], Xv[:,1], c=indices, s=10, marker='s', cmap="Pastel1")
    plt.scatter(Yv[:,0], Yv[:,1], color="black")
    plt.axis('equal')
    
    name = (metric.title() + " metric") if isinstance(metric, str) else metric.__name__
    plt.title(name)

    
def test(cdist_argmin, optimized_cdist_argmin, X, Y):
    true_indices = cdist_argmin(X, Y)
    indices = optimized_cdist_argmin(X, Y)
    if np.all(true_indices == indices):
        print('correct result \o/')
    else:
        print('incorrect result TT')


benchs = pd.DataFrame(columns=['version', 'time (s)', 'speedup'])


def bench(func, X, Y, title):
    t = 0
    N = 20
    for i in range(N):
        t_ = time.time()
        func(X, Y)
        t_ = time.time() - t_
        t += t_
        if t_ > 0.5 and i > 3:
            break
    t /= i

    i = benchs.shape[0]
    if i == 0:
        speedup = 1
    else:
        speedup = benchs.iloc[0]['time (s)'] / t
    benchs.loc[i] = [title, t, speedup]

    print(benchs)