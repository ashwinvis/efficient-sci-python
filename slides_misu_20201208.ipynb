{
 "cells": [
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-06T14:43:02.449486Z",
     "start_time": "2020-12-06T14:43:02.427557Z"
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Writing efficient scientific code in Python\n",
    "\n",
    "<div align='center'>\n",
    "    <img src='img/snakecharmers.jpg'/>\n",
    "</div>\n",
    "    \n",
    "<div align=\"right\">\n",
    "    <b>\n",
    "    Ashwin Vishnu Mohanan<br/>\n",
    "    MISU<br/>\n",
    "    2020-12-07\n",
    "    </b>\n",
    "</div>\n",
    "\n",
    "<div style=\"font-size: 0.8em;\">\n",
    "<a rel=\"license\" href=\"http://creativecommons.org/licenses/by/4.0/\"><img\n",
    "alt=\"Creative Commons License\" style=\"border-width:0; height: 1em;\"\n",
    "src=\"https://i.creativecommons.org/l/by/4.0/88x31.png\" /></a>&nbsp;\n",
    "This presentation is licensed under a \n",
    "<a rel=\"license\"\n",
    "href=\"http://creativecommons.org/licenses/by/4.0/\">Creative Commons Attribution\n",
    "4.0 International License</a>\n",
    "</div>\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Goals for this talk\n",
    "\n",
    "\n",
    "<div>\n",
    "    <div class=\"left\">\n",
    "\n",
    "#### Try to explain\n",
    "\n",
    "- with reasons why Python performance matter\n",
    "- good practices to follow\n",
    "- concrete examples \n",
    "\n",
    "#### Prerequisites\n",
    "- Basic knowledge of Python and NumPy\n",
    "        \n",
    "    </div>\n",
    "    <div class=\"right\">\n",
    "<p style=\"font-size: 0.9rem;font-style: italic;\"><img style=\"display: block;\" src=\"https://live.staticflickr.com/1431/1239399915_b9d5d4a76a_b.jpg\" alt=\"GOAL\" width=\"500em\"><a href=\"https://www.flickr.com/photos/24841463@N00/1239399915\">\"GOAL\"</a><span> by <a href=\"https://www.flickr.com/photos/24841463@N00\">Peter Fuchs</a></span> is licensed under <a href=\"https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=html\" style=\"margin-right: 5px;\">CC BY-NC-SA 2.0</a><a href=\"https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=html\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;\"><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg?image_id=ec932e44-a66d-4573-94e3-5e38afe830f5\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-sa_icon.svg\" /></a></p>\n",
    "       <div/>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Reason #1: Save computing time\n",
    "\n",
    "- *Really fast* computations => shorten iteration time for your research work\n",
    "- Acheive **C- or Fortran-like speed** for your code, while benefiting from all the advantages of the Python world\n",
    "\n",
    "> ![](img/fluidsim.png)\n",
    "> Comparison of the execution times for a 3D case (1283, 10 time steps) between `NS3D` (blue bars) and `fluidsim.solvers.ns3d` (yellow bars). The time consumption is split into different performance-critical functions of the solver.\n",
    "> c.f. [doi:10.5334/jors.239](https://openresearchsoftware.metajnl.com/articles/10.5334/jors.239/)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Reason #2: Lower carbon footprint of your research\n",
    "\n",
    "| | |\n",
    "|---|---|\n",
    "| ![](img/zwart2020.png) | But we can do better ...![](https://raw.githubusercontent.com/paugier/nbabel/master/py/fig/fig_ecolo_impact_transonic.png) |"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Good practices"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tip #0: Algorithmic optimizations go a long way\n",
    "\n",
    "Chosing the right algorithm can give you gains which beat any compiler optimization.\n",
    "\n",
    "#### Examples\n",
    "\n",
    "- Fast Fourier Transform - reduced complexity from $\\mathcal{O} (N^2)$ to $\\mathcal{O}(N\\log{}N)$\n",
    "\n",
    "- Iterative methods for sparse linear systems: Conjugate Gradient, GMRES, BiCGSTAB faster than its predecessors"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "<div>\n",
    "    <div class=\"left\">\n",
    "        <h2>Tip #1: Measure, don't guess</h2>\n",
    "        <br/>\n",
    "        \"Programmers waste enormous amounts of time thinking about, or worrying about, the speed of noncritical parts of their programs, and these attempts at efficiency actually have a strong negative impact when debugging and maintenance are considered. We should forget about small efficiencies, say about 97% of the time: <b>premature optimization is the root of all evil</b>. Yet we should not pass up our opportunities in that critical 3%\" -\n",
    "       <br/>\n",
    "        &nbsp;&nbsp;<small><b>- Donald Knuth</b>, Professor emeritus at Stanford University, Author of <i>The Art of Computer Programming.</i>, Creator of $\\TeX$ typesetting.</small>\n",
    "    </div>\n",
    "    <div class=\"right\">\n",
    "    <p style=\"font-size: 0.9rem;font-style: italic;\"><img style=\"display: block;\" src=\"https://live.staticflickr.com/3064/3066691168_a59e3c0f02_b.jpg\" alt=\"Knuth is My Homeboy!\" width=\"200em\"><a href=\"https://www.flickr.com/photos/99743766@N00/3066691168\">\"Knuth is My Homeboy!\"</a><span> by <a href=\"https://www.flickr.com/photos/99743766@N00\">lumachrome</a></span> is licensed under <a href=\"https://creativecommons.org/licenses/by-sa/2.0/?ref=ccsearch&atype=html\" style=\"margin-right: 5px;\">CC BY-SA 2.0</a><a href=\"https://creativecommons.org/licenses/by-sa/2.0/?ref=ccsearch&atype=html\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;\"><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg?image_id=f7682da0-98b0-4e5c-9053-c078e792190c\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-sa_icon.svg\" /></a></p>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-06T21:04:50.696729Z",
     "start_time": "2020-12-06T21:04:50.612914Z"
    },
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "<div>\n",
    "    <div class=\"left\">\n",
    "        A good target would be to optimize say 20% of the code which consumes 80% of the time\n",
    "        <br/>&nbsp;&nbsp;\n",
    "        <small>See <a href=\"https://en.wikipedia.org/wiki/Pareto_principle\">Pareto principle</a></small>\n",
    "    </div>\n",
    "    <div class=\"right\">\n",
    "        <p style=\"font-size: 0.9rem;font-style: italic;\"><img style=\"display: block;\" src=\"https://live.staticflickr.com/2432/3650673328_2221e1c54b_b.jpg\" alt=\"Pareto Principle Option 2\" width=\"300em\"><a href=\"https://www.flickr.com/photos/39373517@N07/3650673328\">\"Pareto Principle Option 2\"</a><span> by <a href=\"https://www.flickr.com/photos/39373517@N07\">Sleepy Valley</a></span> is licensed under <a href=\"https://creativecommons.org/licenses/by-nc-nd/2.0/?ref=ccsearch&atype=html\" style=\"margin-right: 5px;\">CC BY-NC-ND 2.0</a><a href=\"https://creativecommons.org/licenses/by-nc-nd/2.0/?ref=ccsearch&atype=html\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;\"><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg?image_id=b20c2b85-fc7b-4fca-8587-50945b4244df\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nd_icon.svg\" /></a></p>\n",
    "    </div>\n",
    "</div>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tip #1: Measure, don't guess\n",
    "\n",
    "### How?\n",
    "\n",
    "#### Profile with different granularity\n",
    "- Whole code: time spent per function / method (`cProfile` in standard library)\n",
    "- Line profile: time spent per line (`line-profiler` package)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tip #1: Measure, don't guess\n",
    "\n",
    "#### Benchmark\n",
    "\n",
    "Once the slow parts of the code are identified, run benchmarks. Avoid system jitters, overhead of profiling etc.\n",
    "\n",
    "- Whole code (using `time` in Linux, `timeit` in standard library, or `pyperf` package)\n",
    "- Function or set of functions: micro-benchmark (`timeit`) or something very simple as\n",
    "\n",
    "```py\n",
    "import itertools\n",
    "from time import perf_counter\n",
    "\n",
    "t_elapsed = []\n",
    "for _ in itertools.repeat(None, 20): \n",
    "\n",
    "    t_start = perf_counter()\n",
    "    my_function()  # <-- Function to be benchmarked\n",
    "    t_end = perf_counter()\n",
    "\n",
    "    t_elapsed.append(t_end - t_start) # Post-process the timings\n",
    "```"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-06T21:22:20.211594Z",
     "start_time": "2020-12-06T21:22:20.204726Z"
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tip #2: Tests\n",
    "\n",
    "**Why?** To guarantee a functioning code before and after optimization\n",
    "\n",
    "### Demo: `np.geomspace`\n"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "help(np.geomspace)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-06T21:22:20.211594Z",
     "start_time": "2020-12-06T21:22:20.204726Z"
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Example based testing\n",
    "\n",
    "Using `pytest`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": true
   },
   "outputs": [],
   "source": [
    "!rm -rf tests && mkdir tests"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file tests/test_geomspace_with_pytest.py\n",
    "\n",
    "import numpy as np\n",
    "from numpy.testing import assert_array_equal\n",
    "\n",
    "def test_geomspace_example():\n",
    "    assert_array_equal(np.geomspace(1, 1000, num=4, dtype=int), [1,   10,  100, 1000])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": true,
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "import sys\n",
    "!{sys.executable} -m pytest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-06T21:22:20.211594Z",
     "start_time": "2020-12-06T21:22:20.204726Z"
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "### Property testing\n",
    "\n",
    "Using `pytest` + `hypothesis`"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%file tests/test_geomspace_with_hypothesis.py\n",
    "\n",
    "import numpy as np\n",
    "from numpy.testing import assert_array_equal\n",
    "from hypothesis import given, strategies as st\n",
    "\n",
    "@given(x=st.integers(min_value=1))\n",
    "def test_geomspace_hypothesis(x):\n",
    "    assert_array_equal(np.geomspace(1, x**3, num=4, dtype=int), [1,   x,  x**2, x**3])"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": true,
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "import sys\n",
    "!{sys.executable} -m pytest"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "## Tip #3: Recognize common pitfalls\n",
    "\n",
    "#### Avoid `for` loops whenever possible"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "for _ in range(100_000):  # hundred thousand\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "for _ in range(10_000_000):  # ten million\n",
    "    pass"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "for _ in range(100_000_000):  # hundred million\n",
    "    pass"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Remedies\n",
    "\n",
    "#### *Pythonic* optimizations \n",
    "- For example *list (or set / dictionary) comprehension* or lazy data-structures like *generators*"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "# Instead of ...\n",
    "multiples_of_two = []\n",
    "for x in range(10_000_000):\n",
    "    multiples_of_two.append(2 * x)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "# Use...\n",
    "multiples_of_two = [2 * x for x in range(100_000)]"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "#### Use of efficient data structures\n",
    "\n",
    "- [NumPy](https://numpy.org/) is the defacto API and library for mathematical operations.\n",
    "- Concise, elegant codes."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "multiples_of_two = 2 * np.arange(100_000)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Remedies\n"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "#### Different interpreter\n",
    "\n",
    "- [PyPy](https://pypy.org) is an alternative Python interpreter with **just-in-time (JIT) compiling** capabilities.\n",
    "- Good for repetitive operations in **pure Python**, but **not NumPy** codes."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "## What if none of the above is an option?\n",
    "\n",
    "You ***have to*** implement an algorithm for which no open-source Python implementations are found.\n",
    "\n",
    "> <p style=\"font-size: 0.9rem;font-style: italic;\"><img style=\"display: block;\" src=\"https://live.staticflickr.com/1160/791385521_88ec165c29.jpg\" alt=\"think\"><a href=\"https://www.flickr.com/photos/56932838@N00/791385521\">\"think\"</a><span> by <a href=\"https://www.flickr.com/photos/56932838@N00\">regolare</a></span> is licensed under <a href=\"https://creativecommons.org/licenses/by-nc-nd/2.0/?ref=ccsearch&atype=html\" style=\"margin-right: 5px;\">CC BY-NC-ND 2.0</a><a href=\"https://creativecommons.org/licenses/by-nc-nd/2.0/?ref=ccsearch&atype=html\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;\"><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg?image_id=84789881-9e2c-4ad5-a3ca-7c4bec63e3c0\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nd_icon.svg\"/></a></p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Two ways forward\n",
    "\n",
    "- **Interface** with a library written in C / C++ / Fortran / Rust ...\n",
    "    - Examples: NumPy, SciPy, Tensorflow, PyFFTW\n",
    "    - Either using handwritten extension modules which uses the [Python/C API](https://docs.python.org/3/c-api/index.html) - **not recommended**, avoid when possible.\n",
    "    - Or via, cffi / Cython / pybind11 / f2py / PyO3 ... - **current state-of-the-art**, but steep learning curve."
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "source": [
    "- **Export Extensions**: Python code compiled to C / C++  ...\n",
    "  - Using Cython / Numba / Pythran ... - **topic of today's talk**\n",
    "  - Easy as $\\pi$ to get started...\n",
    "\n",
    "> <p style=\"font-size: 0.9rem;font-style: italic;\"><img style=\"display: block;\" src=\"https://live.staticflickr.com/3233/3355106480_20e4f0e24e_b.jpg\" alt=\"Happy Pi Day (to the 69th digit)!\" width=\"300em\"><a href=\"https://www.flickr.com/photos/64419960@N00/3355106480\" >\"Happy Pi Day (to the 69th digit)!\"</a><span> by <a href=\"https://www.flickr.com/photos/64419960@N00\">Mykl Roventine</a></span> is licensed under <a href=\"https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=html\" style=\"margin-right: 5px;\">CC BY-NC-SA 2.0</a><a href=\"https://creativecommons.org/licenses/by-nc-sa/2.0/?ref=ccsearch&atype=html\" target=\"_blank\" rel=\"noopener noreferrer\" style=\"display: inline-block;white-space: none;margin-top: 2px;margin-left: 3px;height: 22px !important;\"><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc_icon.svg?image_id=ab95d99a-b90e-4304-b9e0-1c6df4596384\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-by_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-nc_icon.svg\" /><img style=\"height: inherit;margin-right: 3px;display: inline-block;\" src=\"https://search.creativecommons.org/static/img/cc-sa_icon.svg\" /></a></p>"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Demo: An application with pairwise distances\n",
    "\n",
    "Courtesy: ***https://github.com/jeremiedbb/tutorial-euroscipy-2019.git***\n",
    "\n",
    "Let ``X`` and ``Y`` be two sets of points. For all points in `X`, find its closest point in `Y` w.r.t some given distance ``d``.\n",
    "\n",
    "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;$\\forall x\\in X, c(x) = \\underset{y\\in Y}{\\operatorname{argmin}}{d(x,y)}$\n",
    "\n",
    "We need to\n",
    "- compute distances between all points in ``X`` and all points in ``Y``.\n",
    "  Done using scipy ``cdist``.\n",
    "- for each point in ``X`` find which point in ``Y`` minimize this distance.\n",
    "  Done using numpy ``argmin``."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "fragment"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from scipy.spatial.distance import cdist\n",
    "from demo import X, Y, plot, test, bench"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "try:\n",
    "    print(f\"Mesh: {X.shape=}, spanning from {X[0,:]=} to {X[-1,:]=}\")\n",
    "    print(f\"Random points: {Y.shape=}\")\n",
    "except SyntaxError:\n",
    "    print(\"Python >= 3.8 specific syntax.\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false,
    "scrolled": false,
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "outputs": [],
   "source": [
    "def cdist_argmin(Xv, Yv, metric):\n",
    "    \"\"\"Computes distances using `scipy.spatial.distance.cdist` and indices of minima using `numpy.argmin`\"\"\"\n",
    "    distances = cdist(Xv, Yv, metric=metric)\n",
    "    argmins = np.argmin(distances, axis=1)\n",
    "    return argmins\n",
    "\n",
    "\n",
    "plot(X, Y, cdist_argmin, \"euclidean\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Defining a custom metric\n",
    "\n",
    "Through the tutorial we'll use the following metric:\n",
    "\n",
    "$$d(x,y) = \\sum_{i=0}^{k}{|x_i - y_i| (i + 1)}$$"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def custom_metric(x, y):\n",
    "    xy_diff = np.abs(x - y)\n",
    "    i = np.arange(1, x.shape[0] + 1)\n",
    "    return xy_diff.dot(i)\n",
    "\n",
    "plot(X, Y, cdist_argmin, custom_metric)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "source": [
    "#### Profile to see the bottleneck"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "X_big = np.random.random_sample((10000, 100))\n",
    "Y_big = np.random.random_sample((100, 100))"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%prun cdist_argmin(X_big, Y_big, custom_metric)"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Baseline"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "def baseline_cdist_argmin(Xv, Yv):\n",
    "    distances = cdist(Xv, Yv, metric=custom_metric)\n",
    "    argmins = np.argmin(distances, axis=1)\n",
    "    return argmins"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, baseline_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "bench(baseline_cdist_argmin, X, Y, \"baseline\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Cython\n",
    "\n",
    "![Cython](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/images/logo_cython.jpg)\n",
    "\n",
    "- Powerful and mature, but a tool for experts! Uses `pyrex` syntax = Python + C-like types\n",
    "- Efficient for C-like code (explicit loops, \"low level\")`\n",
    "\n",
    "Plugging in Python code as is also works..."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext Cython"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%cython\n",
    "import numpy as np\n",
    "\n",
    "def custom_metric(x, y):\n",
    "    xy_diff = np.abs(x - y)\n",
    "    i = np.arange(1, x.shape[0] + 1)\n",
    "    return xy_diff.dot(i)\n",
    "\n",
    "def cython_cdist_argmin(X, Y):\n",
    "    distances = np.empty((X.shape[0], Y.shape[0]))\n",
    "    \n",
    "    for i, x in enumerate(X):\n",
    "        for j, y in enumerate(Y):\n",
    "            distances[i, j] = custom_metric(x, y)\n",
    "    \n",
    "    return np.argmin(distances, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, cython_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bench(cython_cdist_argmin, X, Y, \"cython pure python\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%%cython -a -f\n",
    "import numpy as np\n",
    "\n",
    "def custom_metric(x, y):\n",
    "    xy_diff = np.abs(x - y)\n",
    "    i = np.arange(1, x.shape[0] + 1)\n",
    "    return xy_diff.dot(i)\n",
    "\n",
    "def cython_cdist_argmin(X, Y):\n",
    "    distances = np.empty((X.shape[0], Y.shape[0]))\n",
    "    \n",
    "    for i, x in enumerate(X):\n",
    "        for j, y in enumerate(Y):\n",
    "            distances[i, j] = custom_metric(x, y)\n",
    "    \n",
    "    return np.argmin(distances, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false,
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%%cython -a -f\n",
    "#cython: boundscheck=False, wraparound=False\n",
    "\n",
    "import numpy as np\n",
    "from libc.math cimport fabs\n",
    "\n",
    "\n",
    "cdef double custom_metric(double[:] x, double[:] y):\n",
    "    cdef:\n",
    "        int n = x.shape[0]\n",
    "        double res = 0.0\n",
    "        int i\n",
    "\n",
    "    for i in range(n):\n",
    "        res += (i + 1) * fabs(x[i] - y[i])\n",
    "        \n",
    "    return res\n",
    "\n",
    "\n",
    "def cython_cdist_argmin(double[:, ::1] X, double[:, ::1] Y):\n",
    "    cdef:\n",
    "        int nx = X.shape[0]\n",
    "        int ny = Y.shape[0]\n",
    "        int i, j\n",
    "\n",
    "        double[:, ::1] distances = np.empty((nx, ny))\n",
    "    \n",
    "    for i in range(nx):\n",
    "        for j in range(ny):\n",
    "            distances[i, j] = custom_metric(X[i], Y[j])\n",
    "    \n",
    "    return np.argmin(np.asarray(distances), axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, cython_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bench(cython_cdist_argmin, X, Y, \"cython types loops\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Numba\n",
    "\n",
    " \n",
    "![Numba](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/images/logo_numba.png)\n",
    " \n",
    "- Transpiles to LLVM code\n",
    "- Very simple to use (just add few decorators) 🙂\n",
    "- Implements a subset of Python and Numpy\n",
    "- Parallelization, GPU support"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "# Naive Numba implementation"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "from numba import njit\n",
    "\n",
    "\n",
    "@njit\n",
    "def numba_custom_metric(x, y):\n",
    "    xy_diff = np.abs(x - y)\n",
    "    i = np.arange(1, x.shape[0] + 1).astype(x.dtype)  # <-- Note modification required\n",
    "    \n",
    "    return xy_diff.dot(i)\n",
    "\n",
    "\n",
    "@njit\n",
    "def numba_cdist(X, Y):\n",
    "    nx = X.shape[0]\n",
    "    ny = Y.shape[0]\n",
    "\n",
    "    distances = np.empty((nx, ny))\n",
    "    \n",
    "    for i in range(nx):\n",
    "        for j in range(ny):\n",
    "            distances[i, j] = numba_custom_metric(X[i], Y[j])\n",
    "    \n",
    "    return distances\n",
    "\n",
    "            \n",
    "def numba_cdist_argmin(X, Y):\n",
    "    distances = numba_cdist(X, Y)\n",
    "\n",
    "    return np.argmin(distances, axis=1)  # <-- Numba argmin does not handle the axis parameter"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, numba_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bench(numba_cdist_argmin, X, Y, \"naive numba\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "source": [
    "## Numba fastmath loops"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "-"
    }
   },
   "outputs": [],
   "source": [
    "@njit(fastmath=True)\n",
    "def numba_custom_metric(x, y):\n",
    "    res = 0\n",
    "\n",
    "    for i in range(x.shape[0]):\n",
    "        res += (i + 1) * abs(x[i] - y[i])\n",
    "    \n",
    "    return res\n",
    "\n",
    "\n",
    "@njit(fastmath=True)\n",
    "def numba_cdist(X, Y):\n",
    "    nx = X.shape[0]\n",
    "    ny = Y.shape[0]\n",
    "\n",
    "    distances = np.empty((nx, ny))\n",
    "    \n",
    "    for i in range(nx):\n",
    "        for j in range(ny):\n",
    "            distances[i, j] = numba_custom_metric(X[i], Y[j])\n",
    "    \n",
    "    return distances"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, numba_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bench(numba_cdist_argmin, X, Y, \"numba fastmath loops\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Pythran\n",
    "\n",
    " \n",
    "![](http://www.legi.grenoble-inp.fr/people/Pierre.Augier/images/logo_Pythran.jpeg)\n",
    "\n",
    "- Transpiles a (fairly large) subset of Python+NumPy to efficient C++\n",
    "- Good to optimize _high-level NumPy code_\n",
    "- Extensions never use the Python interpreter (pure C++ ⇒ no GIL)\n",
    "- Can produce C++ that can be used without Python  \n",
    "- Usually **very efficient** (sometimes faster than Julia)\n",
    "- Parallelization via OpenMP"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%load_ext pythran.magic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "%%pythran -O3 -march=native\n",
    "import numpy as np\n",
    "\n",
    "# pythran export pythran_custom_metric(float64[2], float64[2])\n",
    "def pythran_custom_metric(x, y):\n",
    "    xy_diff = np.abs(x - y)\n",
    "    i = np.arange(1, x.shape[0] + 1)\n",
    "    return xy_diff.dot(i)\n",
    "\n",
    "# pythran export pythran_cdist_argmin(float64[:, 2], float64[:, 2])\n",
    "def pythran_cdist_argmin(X, Y):\n",
    "    distances = np.empty((X.shape[0], Y.shape[0]))\n",
    "    \n",
    "    for i, x in enumerate(X):\n",
    "        for j, y in enumerate(Y):\n",
    "            distances[i, j] = pythran_custom_metric(x, y)\n",
    "    \n",
    "    return np.argmin(distances, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, pythran_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": true
   },
   "outputs": [],
   "source": [
    "bench(pythran_cdist_argmin, X, Y, \"pythran pure python\")"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "%%pythran -O3 -march=native\n",
    "import numpy as np\n",
    "\n",
    "# pythran export pythran_custom_metric(float64[2], float64[2])\n",
    "def pythran_custom_metric(x, y):\n",
    "    res = 0\n",
    "\n",
    "    for i in range(x.shape[0]):\n",
    "        res += (i + 1) * abs(x[i] - y[i])\n",
    "    \n",
    "    return res\n",
    "\n",
    "# pythran export pythran_cdist_argmin(float64[:, 2], float64[:, 2])\n",
    "def pythran_cdist_argmin(X, Y):\n",
    "    nx = X.shape[0]\n",
    "    ny = Y.shape[0]\n",
    "\n",
    "    distances = np.empty((nx, ny))\n",
    "    \n",
    "    for i in range(nx):\n",
    "        for j in range(ny):\n",
    "            distances[i, j] = pythran_custom_metric(X[i], Y[j])\n",
    "            \n",
    "    return np.argmin(distances, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, pythran_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "bench(pythran_cdist_argmin, X, Y, \"pythran loops\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "ExecuteTime": {
     "end_time": "2020-12-07T02:12:14.301201Z",
     "start_time": "2020-12-07T02:12:14.166425Z"
    },
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Transonic\n",
    "\n",
    "![](img/transonic.png)\n",
    "\n",
    "- Pure Python package to easily accelerate modern Python-Numpy code with different accelerators (currently Cython, Pythran and Numba).\n",
    "- Just in Time compilation using: `@jit` decorator \n",
    "- Ahead of Time compilation using: `@boost` decorator"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "slideshow": {
     "slide_type": "subslide"
    }
   },
   "outputs": [],
   "source": [
    "import numpy as np\n",
    "from transonic import jit\n",
    "\n",
    "@jit(backend=\"pythran\", native=True, xsimd=True)\n",
    "def transonic_pythran_custom_metric(x, y):\n",
    "    res = 0\n",
    "\n",
    "    for i in range(x.shape[0]):\n",
    "        res += (i + 1) * abs(x[i] - y[i])\n",
    "    \n",
    "    return res\n",
    "\n",
    "@jit(backend=\"pythran\", native=True, xsimd=True)\n",
    "def transonic_pythran_cdist_argmin(X, Y):\n",
    "    nx = X.shape[0]\n",
    "    ny = Y.shape[0]\n",
    "\n",
    "    distances = np.empty((nx, ny))\n",
    "    \n",
    "    for i in range(nx):\n",
    "        for j in range(ny):\n",
    "            distances[i, j] = transonic_pythran_custom_metric(X[i], Y[j])\n",
    "            \n",
    "    return np.argmin(distances, axis=1)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {},
   "outputs": [],
   "source": [
    "test(baseline_cdist_argmin, transonic_pythran_cdist_argmin, X, Y)"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "scrolled": false
   },
   "outputs": [],
   "source": [
    "bench(transonic_pythran_cdist_argmin, X , Y, \"transonic pythran loops\")"
   ]
  },
  {
   "cell_type": "markdown",
   "metadata": {
    "slideshow": {
     "slide_type": "slide"
    }
   },
   "source": [
    "# Thank you for listening!\n",
    "\n",
    "## Summary\n",
    "\n",
    "- Algorithms & good practices\n",
    "- Profiling with `cProfile` and `%prun`\n",
    "- Testing with `pytest` and `hypothesis`\n",
    "- Benchmarking with `time`\n",
    "- Export Python into extensions using Cython, Numba, Pythran and Transonic"
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "metadata": {
    "hide_input": false
   },
   "outputs": [],
   "source": [
    "from demo import benchs\n",
    "\n",
    "benchs.plot.bar(\"version\", \"speedup\", figsize=(8,4))\n",
    "plt.ylim(1, None)"
   ]
  }
 ],
 "metadata": {
  "celltoolbar": "Slideshow",
  "kernelspec": {
   "display_name": "py-efficient-sci-python",
   "language": "python",
   "name": "py-efficient-sci-python"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.8.6"
  },
  "rise": {
   "autolaunch": true,
   "center": true,
   "header": "<img class='header-su-logo' src='img/su_logo_horizontal_english.svg' data-fallback='/img/su_logo_horizontal_english.png' alt='Stockholm university'>",
   "scroll": true,
   "theme": "serif",
   "transition": "fade"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 4
}
