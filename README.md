# efficient-sci-python

Course on writing efficient scientific Python. Objectives:

- Developing a notion of Python's performance
- Good practices: unit tests, profiling, benchmarks
- Overview of the packages which boost performance, from standard library,
  NumPy to compilers like Pythran.

## Try it online!

Preview the Jupyter Notebook contents

[![nbviewer](https://img.shields.io/badge/nbviewer-preview_jupyter_notebook-orange)](https://nbviewer.org/urls/codeberg.org/ashwinvis/efficient-sci-python/raw/branch/main/slides_misu_20201208.ipynb)

Click here to launch the Jupyter Notebook in the cloud:

[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fcodeberg.org%2Fashwinvis%2Fefficient-sci-python/main?filepath=slides_misu_20201208.ipynb)

## Installation

### Using `conda` or  `mamba`

    conda env create --file=environment.yml

### Using `pip`

Setup a [virtual-environment][virtual-environment] and install dependencies:


    pip install -r requirements.txt

Install [Jupyter][Jupyter] notebook / lab either in the same
[virtual-environment][virtual-environment] (no configuration required) or
outside it followed by installing the kernel:

    ipython kernel install --user --name=py-$(basename $VIRTUAL_ENV)

[virtual-environment]: https://packaging.python.org/guides/installing-using-pip-and-virtual-environments/#creating-a-virtual-environment
[Jupyter]: https://jupyter.org
